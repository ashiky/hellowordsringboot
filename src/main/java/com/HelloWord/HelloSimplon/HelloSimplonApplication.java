package com.HelloWord.HelloSimplon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloSimplonApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloSimplonApplication.class, args);
		System.out.println("hello word");
	}

}
